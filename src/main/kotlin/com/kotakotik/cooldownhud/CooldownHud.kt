package com.kotakotik.cooldownhud

import com.mojang.blaze3d.systems.RenderSystem
import com.mojang.blaze3d.vertex.PoseStack
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiComponent
import net.minecraft.client.resources.model.Material
import net.minecraft.resources.ResourceLocation
import net.minecraft.world.inventory.InventoryMenu
import net.minecraftforge.client.event.TextureStitchEvent
import net.minecraftforge.client.gui.ForgeIngameGui
import net.minecraftforge.client.gui.IIngameOverlay
import net.minecraftforge.client.gui.OverlayRegistry
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.IExtensionPoint
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent
import net.minecraftforge.network.NetworkConstants
import org.apache.logging.log4j.LogManager
import thedarkcolour.kotlinforforge.forge.FORGE_BUS
import thedarkcolour.kotlinforforge.forge.LOADING_CONTEXT
import thedarkcolour.kotlinforforge.forge.MOD_BUS
import java.awt.Color
import kotlin.math.roundToInt

const val tickTime = 20

// for some reason when this object is being compiled to a java class and kotlinforforge is not recognizing it correctly, so im gonna make this a class
@Mod(modId)
class CooldownHud {
    init {
        LOADING_CONTEXT.registerExtensionPoint(
            IExtensionPoint.DisplayTest::class.java
        ) {
            IExtensionPoint.DisplayTest(
                { NetworkConstants.IGNORESERVERONLY }
            ) { _, _ -> true }
        }

        logger.info("Hello world, from $displayName, $version!")

        MOD_BUS.register(Events)
        FORGE_BUS.register(ForgeEvents)
    }

    object ForgeEvents {
    }

    object Events {
        @SubscribeEvent
        fun registerOverlays(e: FMLClientSetupEvent) {
            OverlayRegistry.registerOverlayTop(modId, CoolDownHud)
        }

        @SubscribeEvent
        fun atlasLoad(e: TextureStitchEvent.Pre) {
            if (e.atlas.location() == InventoryMenu.BLOCK_ATLAS) {
                e.addSprite(CoolDownHud.exclamationTexture)
            }
        }
    }

    object CoolDownHud : IIngameOverlay {
        //    val cooldownsField = ObfuscationReflectionHelper.findField(ItemCooldowns::class.java, "cooldowns")
//    val endTimeField = ObfuscationReflectionHelper.findField(ItemCooldowns::class.java.classes.first(), "endTime")
        private const val timeToSlideIn = .3f
        private const val xEndPos = 20
        private const val xBasePos = -20
        private const val yBasePos = 30
        private const val xDist = xEndPos - xBasePos
        private const val spaceBetweenCoolDowns = 20
        val exclamationTexture = loc("gui/exclamation")
        private const val counterDepth = 100
        private const val exclamationSize = 12
        private const val exclamationOffset = 25
        private const val exclamationYOffset = -3

        override fun render(gui: ForgeIngameGui?, mStack: PoseStack?, partialTicks: Float, width: Int, height: Int) {
            mStack ?: return
            gui ?: return

            val plr = minecraft.player ?: return
            var y = yBasePos
//        for(i in (1..300)) {
//            minecraft.itemRenderer.renderGuiItem(
//                Items.APPLE.defaultInstance,
//                (i * (sin(plr.cooldowns.tickCount.toDouble() / 30) * 10)).roundToInt(), 30)
//        }
            for ((item, coolDown) in plr.cooldowns.cooldowns) {
                val timeElapsed =
                    (plr.cooldowns.tickCount + (if (minecraft.isPaused) 0f else minecraft.frameTime) - coolDown.startTime) / tickTime
                val x = xBasePos + (timeElapsed / timeToSlideIn).coerceAtMost(1f) * xDist
                val timeLeft = (coolDown.endTime - coolDown.startTime).toFloat() / tickTime - timeElapsed

                y += spaceBetweenCoolDowns

                if (timeElapsed < .8f && timeElapsed % .5f < .3) {
                    mStack.pose {
                        RenderSystem.enableBlend()
                        RenderSystem.setShaderTexture(0, InventoryMenu.BLOCK_ATLAS)

                        GuiComponent.blit(
                            this,
                            x.toInt() + exclamationOffset,
                            y + exclamationYOffset,
                            0,
                            exclamationSize,
                            exclamationSize,
                            Material(InventoryMenu.BLOCK_ATLAS, exclamationTexture).sprite()
                        )

                        RenderSystem.disableBlend();
                    }
                }

//            minecraft.itemRenderer.renderGuiItem(ItemStack(item), 30, 30)
                run {
                    val itemStack = plr.inventory.items.firstOrNull { it.item == item } ?: item.defaultInstance
                    minecraft.itemRenderer.renderAndDecorateItem(
                        itemStack,
                        (x - 17).toInt(),
                        y - spaceBetweenCoolDowns / 2
                    )
                }

                GuiComponent.drawString(
                    mStack,
                    minecraft.font,
                    ((timeLeft * counterDepth).roundToInt().toFloat() / counterDepth).toString(),
                    x.toInt(),
                    y,
                    Color.WHITE.rgb
                )
            }
        }
    }
}

// create location with custom path, and with namespace of your mod id
internal fun loc(id: String) = ResourceLocation(modId, id)

// get minecraft client, will crash on server!
internal inline val minecraft get() = Minecraft.getInstance()

// get your mod's logger
internal inline val logger get() = LogManager.getLogger(modId)

inline fun PoseStack.pose(func: PoseStack.() -> Unit) =
    this.also { it.pushPose(); func(it); it.popPose() }