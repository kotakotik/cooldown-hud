package com.kotakotik.cooldownhud.mixin

import com.kotakotik.cooldownhud.logger
import com.kotakotik.cooldownhud.minecraft
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.item.Item
import net.minecraft.world.item.ItemCooldowns
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo

@Mixin(ItemCooldowns::class)
@Suppress("CAST_NEVER_SUCCEEDS")
class CoolDownSoundPlayer {
    @Inject(method = ["onCooldownEnded"], at = [At("HEAD")])
    fun onCooldownEnded(ci: CallbackInfo) {
        val plr = minecraft.player ?: return logger.info("Could not get player, not playing ding sound")
        minecraft.level?.playSound(
            plr,
            plr.x,
            plr.y,
            plr.z,
            SoundEvents.ARROW_HIT_PLAYER,
            SoundSource.MASTER,
            .3f,
            1f
        )
    }
}

fun ItemCooldowns.getCoolDownPercent(item: Item) =
    getCooldownPercent(item, minecraft.frameTime)