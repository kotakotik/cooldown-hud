@file:Suppress("PropertyName")

import net.minecraftforge.gradle.common.util.RunConfig
import org.spongepowered.asm.gradle.plugins.MixinExtension

// shadow
//import com.github.jengelman.gradle.plugins.shadow.tasks.ConfigureShadowRelocation

buildscript {
    repositories {
        maven(url = "https://maven.minecraftforge.net")
        maven("https://repo.spongepowered.org/maven")
        mavenCentral()
    }
    dependencies {
        classpath("net.minecraftforge.gradle:ForgeGradle:5.+") {
            isChanging = true
        }
        // Make sure this version matches the one included in Kotlin for Forge
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
        // OPTIONAL Gradle plugin for Kotlin Serialization
        classpath("org.jetbrains.kotlin:kotlin-serialization:1.6.10")
        classpath("org.spongepowered:mixingradle:0.7-SNAPSHOT")
    }
}

plugins {
    kotlin("jvm") version "1.6.10"
    java
    id("net.minecraftforge.gradle") version "5.+"
    // shadow
//    id("com.github.johnrengelman.shadow") version "7.1.2"
}

apply(plugin = "org.spongepowered.mixin")
//apply(plugin = "kotlin")
//apply(plugin = "kotlinx-serialization")

val modid = "cooldownhud"

val mod_version: String by project
val mappings_version: String by project
val forge_version: String by project
val display_name: String by project
// devauth
//val devauth_version: String by project

apply(from = "https://raw.githubusercontent.com/thedarkcolour/KotlinForForge/site/thedarkcolour/kotlinforforge/gradle/kff-3.1.0.gradle")

version = mod_version
group = "com.kotakotik.$modid"

java.toolchain.languageVersion.set(JavaLanguageVersion.of(17))
//kotlin.jvmToolchain {}

val generatedRoot = "$buildDir/generated"
val generatedPath = "$generatedRoot/${group.toString().replace(".", "/")}"

sourceSets.main {
    java {
        srcDir(generatedRoot)
    }
}

minecraft.apply {
    // Change to your preferred mappings
    mappings("official", mappings_version)
    // Add your AccessTransformer
    accessTransformer(file("src/main/resources/META-INF/accesstransformer.cfg"))

    runs {
        val default = RunConfig(project, "default").apply {
            property("forge.logging.markers", "SCAN,LOADING,CORE")
            property("forge.logging.console.level", "debug")

            property("mixin.env.remapRefMap", "true")
            property("mixin.env.refMapRemappingFile", "${projectDir}/build/createSrgToMcp/output.srg")
            args(
                "-mixin.config=${modid}.mixins.json"
            )

            mods {
                create(modid) {
                    source(sourceSets.main.get())
                }
            }
        }

        create("client") {
            parent(default)
            workingDirectory(project.file("run"))
            // uncomment to create a devauth config (see devauth readme https://github.com/DJtheRedstoner/DevAuth#readme)
//            jvmArgs("-Ddevauth.enabled=true")
        }

        val server = create("server") {
            parent(default)
            workingDirectory(project.file("run/server"))
        }

        // uncomment for a second server task
//        create("server2") {
//            parent(server)
//            workingDirectory(project.file("run/server2"))
//        }
    }
}

//fun generatedPath(name: String) = "$buildDir/generated/$name/${group.toString().replace(".", "/")}/$name/"
fun generatedFile(filename: String) = File("${generatedPath}/$filename.kt")

fun Task.generatedComment() = "// Automatically generated from gradle task ${this.name}\n\n"

val generateConsts by tasks.registering {
    group = "other"

    val outputPackage = ""
    val output = generatedFile("${outputPackage.replace(".", "/")}/${modid}Constants")

    outputs.file(output.path)

    // note: it is very easy to inject code into these constants, so you should not put untrusted constants here
    val strConstants = listOf(
        "version" to version,
        "modId" to modid,
        "displayName" to display_name
    )

    val constants = listOf(
        *strConstants.map { it.first to "\"${it.second}\"" }.toTypedArray()
    )

    doLast {
        val code = "package ${project.group}${
            if (outputPackage.isBlank()) "" else "." + outputPackage.replace(
                "/",
                "."
            )
        }\n\n" + generatedComment() + constants.joinToString("\n") { "const val ${it.first} = ${it.second}" }
        output.writeText(code)
    }
}

val library = configurations.getAt("library")
// shadow
//val shade: Configuration by configurations.creating { }
//val relocateShadowJar =
//    tasks.register<ConfigureShadowRelocation>("relocateShadowJar") {
//        target = tasks.shadowJar.get()
//        prefix = "${project.group}.repack"
//    }

tasks {
    // shadow
//    shadowJar {
//        configurations.clear()
//        configurations.add(shade)
//        dependsOn(relocateShadowJar)
//        minimize()
//    }

    jar {
        finalizedBy("reobfJar")
    }

    clean {
        delete(generatedPath)
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    dependsOn(generateConsts)
}

repositories {
    // devauth
//    maven { url = uri("https://pkgs.dev.azure.com/djtheredstoner/DevAuth/_packaging/public/maven/v1" )}
    maven("https://maven.blamejared.com/")
}

dependencies {
    // Use the latest version of Minecraft Forge
    minecraft("net.minecraftforge:forge:$forge_version")
    fun add(path: String, block: ExternalModuleDependency.() -> Unit = {}) {
        library(path, block)
        // shadow
//        shade(path, block)
        implementation(path, block)
    }

    // devauth
//    runtimeOnly("me.djtheredstoner:DevAuth-forge-latest:${devauth_version}")

    annotationProcessor("org.spongepowered:mixin:0.8.5:processor")
}

tasks.withType<Jar> {
    archiveBaseName.set(modid)

    manifest {
        attributes(
            mapOf(
                "Specification-Title" to modid,
                "Specification-Vendor" to "${modid}sareus",
                "Specification-Version" to "1",
                "Implementation-Title" to project.name,
                "Implementation-Version" to project.version.toString(),
                "Implementation-Vendor" to "${modid}sareus",
                "Implementation-Timestamp" to `java.text`.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .format(`java.util`.Date()),
                "MixinConfigs" to "${modid}.mixins.json"
            )
        )
    }
}

extensions.getByType<MixinExtension>().add(sourceSets.main.get(), "${modid}.refmap.json")
